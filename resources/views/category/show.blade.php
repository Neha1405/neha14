@extends('layouts.master_layout')
@section('content')
    <form>
    <h1>All Categories</h1>

    <table class="table table-dark" style="margin-top:50px">
    <tr>
      <th>Name</th>
      <th>Description</th>
      <th>Blogs({{$category->blogs->count()}})</th>
      

    </tr>
    <tr>
    <td>{{ $category->name }}
    </td>
    <td>{{ $category->description }}
    </td>
    <td>
    @foreach($category->blogs as $blog)
    <a href="/blogs/{{$blog->id}}/show" style="text-decoration:none; color:#B8860B">-{{$blog->title}}</a>
    <br>
    <br>
    @endforeach
  
    </td>
  
  
    </tr>

    
    </form>


  </table>
  <a href="/categories" class="btn btn-outline-success"> Back to all Category</a>
  </form>
    @endsection