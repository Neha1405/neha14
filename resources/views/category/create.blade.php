@extends('layouts.master_layout')
@section('content')
    
    <form method="POST" action="/categories/store"> 
    @csrf()
    <h1>Create Category:</h1>
  <div class="form-group">
    <label for="exampleInputEmail1">Name</label>
    <input type="text" class="form-control" name="name"  placeholder="Enter Name">
  </div>
  <div class="form-group">
    <label for="">Description</label>
    <textarea class="form-control"  name="description" placeholder="Add your description" style="height:250px;"></textarea>
  </div>
  
  <button type="submit" class="btn btn-primary">Submit</button>
  <br>
  <a href="/categories" class="btn btn-outline-success" style="margin-top:20px"> Back to all Categories</a>
</form>
    
    @endsection