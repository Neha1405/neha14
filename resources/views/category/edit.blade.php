@extends('layouts.master_layout')
@section('content')
    
    <form method='POST' action="/categories/{{$category->id}}/update"> 
    @csrf()
    @method('PUT')
    <h1>Edit Category:</h1>
  <div class="form-group">
    <label for="exampleInputEmail1">Name</label>
    <input type="text" class="form-control" name="name"  value="{{$category->name}}">
  </div>
  <div class="form-group">
    <label for="">Description</label>
    <textarea class="form-control"  name="description"  style="height:250px;">{{$category->description}}</textarea>
  </div>
  
  <a href="/categories"></a><button type="submit" class="btn btn-primary">Update</button>
  <br>
  <a href="/categories" class="btn btn-outline-success" style="margin-top:20px"> Back to all Blogs</a>
</form>
    
    @endsection