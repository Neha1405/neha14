@extends('layouts.master_layout')
@section('content')
   <form action="" method="GET">
    <input type="text" placeholder="Search Category" name="searchB" value="{{$searchB}}" style="width:200px">
    <button type="submit"><span class="fa fa-search form-control-feedback"></span></button>
    </form>
    <h1>All Categories</h1>
    <a href="/categories/create" class="btn btn-info" style="margin-left:700px">Add new category</a>

    <table class="table table-dark" style="margin-top:50px">

    <tr>
      <th>Name</th>
      <th>Blog</th>
      <th>Created on</th>
      <th>Updated on</th>
      <th>Action</th>
    
    </tr>

    
    @foreach($categories as $category)
<tr>
    <td>{{ $category->name}}</td>

    <td>
    {{ $category->blogs->count()}}

    </td>
    <td> {{ $category->created_at }}</td>
    <td> {{ $category->updated_at }} </td>
    <td class="form-group row" >
    <a href="/categories/{{$category->id}}/show"><i class="fas fa-eye"  style="margin-left:50px;color:#00BFFF;"></i></a>
    <a href="/categories/{{$category->id}}/edit" ><i class="fas fa-edit" style="margin-left:50px;color:#00BFFF;"></i></a>
    
    <form action="categories/{{$category->id}}/destroy" method="POST">
    @csrf()
    @method('delete')
    <button style="margin-left:40px; color:#00BFFF; background-color:transparent; border:none" class="fas fa-trash"></button></td>
    
    </form>
    
    
    </tr>
    @endforeach

  </table>
  {{$categories->appends($_GET)->links()}}
  
   @endsection