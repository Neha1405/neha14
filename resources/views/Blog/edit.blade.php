@extends('layouts.master_layout')
@section('content')
    
    <form method='POST' action="../../blogs/{{$blog->id}}/update"> 
    @csrf()
    @method('PUT')
    <h1>Edit Blog:</h1>
  <div class="form-group">
    <label for="exampleInputEmail1">Name</label>
    <input type="text" required="" class="form-control" name="title" value="{{$blog->title}}" >
  </div>
  <div class="form-group">
    <label for="">Description</label>
    <textarea class="form-control" required="" name="description"  style="height:250px;" >{{$blog->description}}</textarea>
  </div>
  <div>
  <select class="custom-select" required="" name="blog_category">
  <option selected>--Select Category--</option>
    @foreach($categories as $category)
        <option 
        @if($category->id==$blog->category_id)
        selected
        @endif
        value="{{ $category->id }}">{{ $category->name }}</option>
        @endforeach
    </select>
    
    </select>
  </div>
  
  <a href="/categories"></a><button type="submit" class="btn btn-primary" style="margin-top:30px">Update</button>
</form>
<a href="/blogs" class="btn btn-outline-success"> Back to all Blogs</a>
    
    @endsection