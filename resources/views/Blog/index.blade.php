@extends('layouts.master_layout')
@section('content')
<form action="" method="GET">
    <input type="text" placeholder="Search Blog" name="searchB" value="{{$searchB}}" style="width:200px">
    
    <input type="text" placeholder="Search Category" name="searchC" value="{{$category}}" style="width:200px; margin-top:-35px">
    <button type="submit"><span class="fa fa-search form-control-feedback"></span></button>
    </form>
  

    <h1 style="margin-top:30px">All Blogs</h1>
    <a href="/blogs/create" class="btn btn-info" style="margin-left:700px; margin-top:-50px">Add new Blog</a>
  
    <table class="table table-dark" style="margin-top:20px">

    <tr>
      <th>Blog Title</th>
      <th>Category</th>
      <th>Action</th>
    
    </tr>

    
@foreach($blogs as $blog)
<tr>
    <td>{{ $blog->title }}</td>
    <td>{{ $blog->category->name }}</td>
    <td class="form-group row" >
    <a href="blogs/{{$blog->id}}/show"><i class="fas fa-eye" style="margin-left:50px;color:#00BFFF"></i></a>
    <a href="../blogs/{{$blog->id}}/edit"><i class="fas fa-edit"style="margin-left:50px;color:#00BFFF"></i></a>
     
     <form action="blogs/{{$blog->id}}/destroy" method="POST">
    @csrf()
    @method('DELETE')
    <button type="submit" style="margin-left:40px;color:#00BFFF; background-color:transparent; border:none" class="fas fa-trash"></button>
   
  
    </form>
    </td>
    
    </tr>
    @endforeach

  </table>
    {{$blogs->appends($_GET)->links()}}
 
    @endsection