@extends('layouts.master_layout')
@section('content')
    <form method="POST" action="/blogs/store"> 
    @csrf()
    <h1>Create Blog:</h1>
  <div class="form-group">
    <label for="exampleInputEmail1">Title</label>
    <input type="text" required="" class="form-control" name="title"  placeholder="Enter Name">
  </div>
  <div class="form-group">
    <label for="">Description</label>
    <textarea class="form-control" required="" name="description" placeholder="Add your description" style="height:250px;"></textarea>
  </div>
  <div>
  <select class="custom-select" required="" name="blog_category">
    <option selected>--Select Category--</option>
    @foreach($categories as $category)
        <option value="{{ $category->id }}">{{ $category->name }}</option>
        @endforeach
    </select>

    </div>
<div>
<select multiple class="custom-select"  name="tag_id[]" style="margin-top:30px">
    <option selected>--Select Tag--</option>
    @foreach($tags as $tag)
        <option value="{{ $tag->id }}">{{ $tag->name }}</option>
        @endforeach
    </select>

</div>

  </div>
  
  <button type="submit" class="btn btn-primary" style="margin-top:-90px;margin-left:20px">Submit</button>
  <br>
  <a href="/blogs" class="btn btn-outline-success" style="margin-top:-45px;margin-left:20px"> Back to all Blogs</a>
</form>
    
   @endsection