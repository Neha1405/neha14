@extends('layouts.master_layout')
@section('content')
    <form>
    <h1>All Blogs</h1>

    <table class="table table-dark" style="margin-top:50px">
    <tr>
      <th>Title</th>
      <th>Category</th>
      <th>Description</th>
      <th>Tag</th>
    
    </tr>
    <tr>
    <td>
    {{ $blog->title }}
    </td>
    <td>
    <a href="/categories/{{$blog->category->id}}/show" style="text-decoration:none; color:aquamarine" > {{ $blog->category->name}} </a>
    </td>
    <td>
    {{ $blog->description}}
    </td>
    <td>
    @foreach($blog->tags as $tag)
    -{{$tag->name}}
    <br>
    @endforeach
    
    </td>
    
    </tr>
    </table>
    <a href="/blogs" class="btn btn-outline-success"> Back to all Blogs</a>
    @endsection