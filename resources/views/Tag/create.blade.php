@extends('layouts.master_layout')
@section('content')
    <form method="POST" action="/tags/store"> 
    @csrf()
    <h1>Create Tag:</h1>
  <div class="form-group">
    <label for="exampleInputEmail1">Name</label>
    <input type="text" class="form-control" name="name"  placeholder="Enter Tag">
  </div>
  <button type = "submit" class="btn btn-primary">Submit</button>
  <br>
  <a href="/tags" class="btn btn-outline-success" style="margin-top:20px"> Back to all Tags</a>
  </form>
  @endsection
