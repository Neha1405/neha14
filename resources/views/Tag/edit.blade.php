@extends('layouts.master_layout')
@section('content')
    <form method='POST' action="/tags/{{$tag->id}}/update"> 
    @csrf()
    @method('PUT')
    <h1>Edit Tag:</h1>
  <div class="form-group">
    <label for="exampleInputEmail1">Title</label>
    <input type="text" class="form-control" name="name" value="{{$tag->name}}">
  </div>
  <a href="/tags"></a><button type="submit" class="btn btn-primary">Update</button>
  <br>
  <a href="/tags" class="btn btn-outline-success" style="margin-top:20px"> Back to all Tags</a>
  </form>

  @endsection