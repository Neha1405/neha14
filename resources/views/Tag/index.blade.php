@extends('layouts.master_layout')
@section('content')
<form action="" method="GET">
    <input type="text" placeholder="Search Tag" name="searchB" value="{{$searchB}}" style="width:200px">
    <button type="submit"><span class="fa fa-search form-control-feedback"></span></button>

    </form>
    <h1>All Tags</h1>
    <a href="/tags/create" class="btn btn-info" style="margin-left:700px">Add new Tag</a>

    <table class="table table-dark" style="margin-top:50px">

    <tr>
      <th>Name</th>
      <th>Action</th>
    
    </tr>

@foreach($tags as $tag)
<tr>
    <td>{{$tag->name}}</td>
    <td class="form-group row" >
    <a href="tags/{{$tag->id}}/edit" ><i class="fas fa-edit" style="margin-left:50px;color:#00BFFF"></i></button></a>
    <form action="tags/{{$tag->id}}/destroy" method="POST">
    @csrf()
    @method('delete')
    <button style="margin-left:40px;color:#00BFFF; background-color:transparent; border:none" class="fas fa-trash"></button></td>
    
    </form>
    
    
    </tr>
    @endforeach

  </table>
  {{$tags->appends($_GET)->links()}}
    @endsection